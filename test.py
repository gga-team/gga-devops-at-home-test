import json
import logging

log = logging.getLogger()
log.setLevel(logging.INFO)

def lambda_handler(event, context):

    response = "Hello World"
    log.info(response)
    print(response)

    return {
        'statusCode': 200,
        'body': json.dumps(response)
    }

# EOF