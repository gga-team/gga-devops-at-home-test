# DevOps At-Home Code Test #

Welcome to the GGA DevOps Take Home Test.

This should take a couple hours to complete.

Please use the weekend to setup the resources asked, and be prepared to talk about them during your interview.

We would like to see how you follow instructions, think about what needs to be done, and see a working instance of this.

### Task

Follow the steps below to take source code from repo to deployment.

1. Create a Free AWS account or use an existing one.

2. Create an AWS Lambda.

    a. Provide proper Lambda permissions to execute.

3. Install Jenkins and AWS CLI on your local machine.

4. Create a Jenkins pipeline that:

    a. Checks out this python project ```git clone https://bitbucket.org/gga-team/gga-devops-at-home-test.git```

    b. Run a test by calling the python script [test.py](test.py) to ensure it produces the correct output "Hello World".

    c. If test successful, deploys it to AWS Lambda using AWS CLI.

5. Test Lambda endpoint to verify output is "Hello World".

